export const TABLES = [
    {
        id: 1,
        name: 'EMPLOYEE',
        alias: 'emp'
    },
    {
        id: 2,
        name: 'DEPARTMENT',
        alias: 'dept'
    },
    {
        id: 3,
        name: 'LOCATION',
        alias: 'loc'
    },
    {
        id: 4,
        name: 'DEPENDENTS',
        alias: 'depd'
    },
    {
        id: 5,
        name: 'COUNTRIES',
        alias: 'ctry'
    },
    {
        id: 6,
        name: 'REGION',
        alias: 'rgn'
    },
    {
        id: 7,
        name: 'JOBS',
        alias: 'job'
    }
]

export const RELATIONS = [
    {
        id: 1,
        leftTable: 1,
        rightTable: 2,
        type: 'inner',
        joinConditions: [
            {
                leftExpr: {
                    colName: 'DEPT_ID',
                },
                rightExpr: {
                    colName: 'DEPT_ID',
                },
                operator: '='
            }
        ]
    },
    {
        id: 2,
        leftTable: 2,
        rightTable: 3,
        type: 'left',
        joinConditions: [
            {
                leftExpr: {
                    colName: 'LOC_ID',
                },
                rightExpr: {
                    colName: 'LOC_ID',
                },
                operator: '='
            }
        ]
    },
    {
        id: 3,
        leftTable: 1,
        rightTable: 4,
        joinConditions: [
            {
                leftExpr: {
                    colName: 'EMP_ID',
                },
                rightExpr: {
                    colName: 'EMP_ID',
                },
                operator: '='
            }
        ]
    },
    {
        id: 4,
        leftTable: 1,
        rightTable: 7,
        joinConditions: [
            {
                leftExpr: {
                    colName: 'JOB_ID',
                },
                rightExpr: {
                    colName: 'JOB_ID',
                },
                operator: '='
            }
        ]
    },
    {
        id: 5,
        leftTable: 3,
        rightTable: 5,
        joinConditions: [
            {
                leftExpr: {
                    colName: 'CTRY_ID',
                },
                rightExpr: {
                    colName: 'CTRY_ID',
                },
                operator: '='
            }
        ]
    },
    {
        id: 6,
        leftTable: 5,
        rightTable: 6,
        joinConditions: [
            {
                leftExpr: {
                    colName: 'RGN_ID',
                },
                rightExpr: {
                    colName: 'RGN_ID',
                },
                operator: '='
            }
        ]
    },
    {
        id: 7,
        leftTable: 1,
        rightTable: 3,
        joinConditions: [
            {
                leftExpr: {
                    colName: 'LOC_ID',
                },
                rightExpr: {
                    colName: 'LOC_ID',
                },
                operator: '='
            }
        ]
    }
]

export const ATTRIBUTES = [
    {
        id: 1,
        name: 'EMP_NAME',
        display: 'Employee Name',
        alias: 'empName',
        table: 1
    },
    {
        id: 10,
        name: 'EMP_EMAIL',
        display: 'Employee Email',
        alias: 'empEmail',
        table: 1
    },
    {
        id: 2,
        name: 'DEPT_NAME',
        display: 'Deparment Name',
        alias: 'deptName',
        table: 2
    },
    {
        id: 3,
        name: 'LOC_NAME',
        display: 'Department Location',
        alias: 'locName',
        table: 3
    },
    {
        id: 4,
        name: 'EMP_SALARY',
        display: 'Employee Salary',
        alias: 'empSalary',
        table: 1
    },
    {
        id: 5,
        name: 'JOB_TITLE',
        display: 'Job Title',
        alias: 'jobTitle',
        table: 7
    },
    {
        id: 6,
        name: 'CTRY_NAME',
        display: 'Country Name',
        alias: 'jobTitle',
        table: 5
    },
    {
        id: 7,
        name: 'RGN_NAME',
        display: 'Region Name',
        alias: 'rgnName',
        table: 6
    },
    {
        id: 8,
        name: 'STREET_ADDR',
        display: 'Street Address',
        alias: 'streetAddr',
        table: 3
    },
    {
        id: 9,
        name: 'RELATIONSHIP',
        display: 'Dependent Relationship',
        alias: 'depRel',
        table: 4
    }
]