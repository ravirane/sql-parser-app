import { ATTRIBUTES } from './TestData'
import { Box } from "./Box";

const LeftPanel = () => {
    return (
        <div className='left-panel'>
            {ATTRIBUTES.map((attr, i) => <Box key={i} {...attr} />)}
        </div>
    )
}

export default LeftPanel