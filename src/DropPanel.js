import React, { useState } from 'react';
import { useDrop } from 'react-dnd';
import { ItemTypes } from './constants';
import { Button } from 'antd';

export const DropPanel = ({ onDropAttr, onClearAttr }) => {

    const [ items, setItems ] = useState([]);

    const onDrop = (item) => {
        const newItems = items.concat(item.display)
        setItems(newItems)
        onDropAttr(item.display)
    }

    const [{ canDrop, isOver }, drop] = useDrop({
        accept: ItemTypes.ATTR,
        drop: (item) => onDrop(item),
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    });

    const onClear = () => {
        setItems([])
        onClearAttr()
    }

    return (
    <div ref={drop} className='drop-target'>
        <Button className='clear-btn' onClick={onClear}>{`Clear`}</Button>
        <div className='drop-content'>
            {
                items.map((item,i) => <div key={i}>{item}</div>)
            }
        </div>
    </div>
        );
};
