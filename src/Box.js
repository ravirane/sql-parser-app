import React from 'react';
import { useDrag } from 'react-dnd';
import { ItemTypes } from './constants';
const style = {
    border: '1px dashed gray',
    backgroundColor: 'white',
    padding: '0.5rem 1rem',
    marginRight: '1.5rem',
    marginBottom: '1.5rem',
    cursor: 'move',
    display: 'inline-block'
};
export const Box = ({ id, name, display }) => {
    const [{ isDragging }, drag] = useDrag({
        item: { id, name, display, type: ItemTypes.ATTR },
        end: (item, monitor) => {
            const dropResult = monitor.getDropResult();
            if (item && dropResult) {
             //   alert(`You dropped ${item.display} into ${dropResult.name}!`);
            }
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    });
    const opacity = isDragging ? 0.4 : 1;
    return (<div ref={drag} style={{ ...style, opacity }}>
			{display}
		</div>);
};
