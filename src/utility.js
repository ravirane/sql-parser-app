export function getTables (obj) {
   const jsonData = {...obj} ;
   const tables = [];

   if (jsonData.hasOwnProperty('value') && jsonData.value.hasOwnProperty('from')
   && jsonData.value.from.hasOwnProperty('value')
   ){
      if(Array.isArray(jsonData.value.from.value) 
      && jsonData.value.from.value.length != 0 
      && jsonData.value.from.value[0].hasOwnProperty('value')){
         console.log(jsonData.value.from.value[0].value);
         const allJoins = getAllJoins(jsonData.value.from.value[0].value);
         const decideArrayLength = allJoins.length === 1 ? allJoins.length : allJoins.length-1;
         for(let i = decideArrayLength; i > 0 ; i--){            
            tables.push({
               table1: allJoins[i] ? allJoins[i].table.value.value : allJoins[i-1].table.value.value,
               table2: allJoins[i] ? allJoins[i-1].table.value.value : '',
               typeOfJoin: allJoins[i-1].typeOfJoin
            })            
         }         
      }
   }
   return tables;
}



function getAllJoins(data){
   let allJoins = [];
   function recurse(obj){
      if(!obj.hasOwnProperty('left')){
         allJoins.push({table: obj, typeOfJoin: null});
         return;
      }         
      else{
         allJoins.push({table: obj.right, typeOfJoin:obj.hasOwnProperty('leftRight')  ? obj.leftRight : obj.type});
         recurse(obj.left);
      }
   }
   recurse(data);
   return allJoins;
}
