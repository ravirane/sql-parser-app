import './App.css';
import parser from './parser/sqlParser'
import { Row, Col } from 'antd';
import { Input, Button } from 'antd';
import 'antd/dist/antd.css';
import { useState } from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'
import { getTables }  from'./utility';

const { TextArea } = Input;
export function TableGenerator() {
  const [sqlString, setSqlString] = useState('');
  const [tables, setTables] = useState([]);

  const onSqlChange = (evt) => {
    setSqlString(evt.target.value)
  }  

  const getTablesFromJSON = () => {
    const ast = parser.parse(sqlString);
    const extractedTables = getTables(ast);
    setTables(extractedTables);
  }

  return (
      <div className="App">
        <DndProvider backend={HTML5Backend}>
          <Row>
            <Col span={23}>
              <div className="section-header">SQL Query</div>
              <TextArea value={sqlString} onChange={onSqlChange} className="parser-text"  autoSize />
            </Col>
          </Row>
        </DndProvider>
        <Row>
          <Col span={24}>
            <Button type="primary" onClick = {getTablesFromJSON} className="block-button">GET TABLES</Button>
          </Col>
        </Row>
        {
          tables.length !== 0 && <Row>
          <Col span={8} className="table-header">Table 1</Col>
          <Col span={8} className="table-header">Table 2</Col>
          <Col span={8} className="table-header">Join Type</Col>
        </Row>
        }
        
        <Row>
          {
            tables.map(i=>{
              return(
              <>
                <Col span={8} className="table-cell">{i.table1}</Col>
                <Col span={8} className="table-cell">{i.table2}</Col>
                <Col span={8} className="table-cell">{i.typeOfJoin}</Col>
              </>);
            })
          }            
        </Row>
      </div>
  );
}

