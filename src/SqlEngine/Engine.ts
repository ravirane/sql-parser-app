import Bfs from './Bfs'
import { buildContext } from './context'
import { from } from './From'
import { QueryType, EngineInput } from './Types'

let query: QueryType = {
    nodeType: "Main",
    value: {
        type: 'Select',
        selectItems: {
            type: 'SelectExpr',
            value:[]
        },
        from: {}
    }
}


const getColumnTemplate = (colName: string, colAlias: string, tableAlias: string) => {
    return {
        type: 'Identifier',
        value: `${tableAlias}.${colName}`,
        alias: `\"${colAlias}\"`,
        hasAs: true
    }
}


export default function Engine(data: EngineInput) {
    const inputData = data
    let context = buildContext(inputData)
    let selectedCols: string[] = []
    let tables: number[] = []
    let bfs = Bfs()

    const selectAttr = (displayName: string) => {
        resetQuery()
        selectedCols.push(displayName)
        selectedCols.forEach(selectedCol => {
            const { name: colName, display: alias, table: tableId } = context.attributeMap[selectedCol]
            if (!tables.includes(tableId)) {
                tables.push(tableId)
            }
            const table = context.tableMap[tableId]
            query.value.selectItems.value.push(getColumnTemplate(colName, alias, table.alias))
        })
        bfs.extractTableRelation(tables, context)
        query.value.from = from(context).buildJoins()
        console.log(JSON.stringify(query))
    }


    const getSql = () => query

    const resetQuery = () => {
        context = buildContext(inputData)
        tables = []
        bfs.reset()

        query = {
            nodeType: "Main",
            value: {
                type: 'Select',
                selectItems: {
                    type: 'SelectExpr',
                    value:[]
                },
                from: {}
            }
        }
    }

    const reset = () => {
        resetQuery()
        selectedCols = []
    }

    return {
        selectAttr,
        getSql,
        reset
    }
}