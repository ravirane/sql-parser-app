export type TableType = {
    id: number
    name: string
    alias: string
}

export type AttributeType = {
    id: number
    name: string
    display: string
    alias: string
    table: number
}

export type RelationType = {
    id: number
    leftTable: number
    rightTable: number
    type: string
    joinConditions: [
        {
            leftExpr: {
                colName: string
            },
            rightExpr: {
                colName: string
            },
            operator: string
        }
    ]
}

export type TableGraphType = {
    [key: number]: number[]
}

export type JoinTable = {
    leftTable: string
    rightTable: string
    conditions: any,
    type: string
}

export type SingleTable = {
    table: string 
}

export type ContextType = {
    tableMap: {
        [key: number]: TableType
    }
    tableIdMap: {
        [key: string]: number

    }
    attributeMap: {
        [key: string]: AttributeType
    }
    tableGraph: TableGraphType,
    joinConditionMap: {
        [key: string]: any
    },
    queryTablesMap?: { [key: number]: number },
    queryTables?: number[],
    joinConditions: (JoinTable | SingleTable) []
}


export type QueryType = {
    nodeType: string,
    value: {
        type: string,
        selectItems: {
            type: string,
            value: ColumnTemplateType[]
        },
        from: any
    }
}


export type ColumnTemplateType = {
    type: string
    value: string
    alias: string
    hasAs: boolean
}

export type EngineInput = {
    tables: TableType[]
    relations: RelationType[]
    attributes: AttributeType[]
}