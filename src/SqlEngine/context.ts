import { ContextType, EngineInput, TableType, AttributeType, RelationType } from './Types'

const context: ContextType = {
    tableMap: {},
    tableIdMap: {},
    attributeMap: {},
    tableGraph: {},
    joinConditionMap: {},
    joinConditions: []
}

const buildAttributeMap = (attributes: AttributeType[]) => {
    attributes.forEach(attr => {
        context.attributeMap[attr.display] = attr
    })
}

const buildTableMap = (tables: TableType[]) => {
    tables.forEach(tab => {
        context.tableMap[tab.id] = tab
        context.tableIdMap[tab.name] = tab.id
    });

}

const buildGraph = (relations: RelationType[]) => {
    relations.forEach(rel => {
        if (context.tableGraph[rel.leftTable]) {
            context.tableGraph[rel.leftTable].push(rel.rightTable)
        } else {
            context.tableGraph[rel.leftTable] = [rel.rightTable];
        }

        if (context.tableGraph[rel.rightTable]) {
            context.tableGraph[rel.rightTable].push(rel.leftTable)
        } else {
            context.tableGraph[rel.rightTable] = [rel.leftTable];
        }
        context.joinConditionMap[rel.leftTable + '-' + rel.rightTable] = rel
        context.joinConditionMap[rel.rightTable + '-' + rel.leftTable] = rel
    })
}


export function buildContext(data: EngineInput) {
    buildTableMap(data.tables)
    buildAttributeMap(data.attributes)
    buildGraph(data.relations)
    return context
}