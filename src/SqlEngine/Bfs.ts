import { ContextType, TableGraphType, JoinTable, SingleTable } from './Types'



export default function Bfs() {
    let queryTablesMap: { [key: number]: number } = {}
    let queryTables: number[] = []
    let joinConditions: (JoinTable|SingleTable)[] = []

    const bfs = (srcTable: number, destTable: number, tGraph: TableGraphType) => {
        const queue = [srcTable]
        const visited = [srcTable]
        let len = 1;
        const parentMap: {[key: number]: number} = {}
        while(queue.length > 0) {
            const size = queue.length
            for(let i = 0; i < size; i++) {
                const tableId = queue.shift()
                if(tableId && tGraph[tableId]) {
                    for(let j = 0; j < tGraph[tableId].length; j++) {
                        if (tGraph[tableId][j] === destTable) {
                            parentMap[tGraph[tableId][j]] = tableId;
                            return {
                                len,
                                parentMap
                            }
                        }
                        if (!visited.includes(tGraph[tableId][j])) {
                            parentMap[tGraph[tableId][j]] = tableId;
                            queue.push(tGraph[tableId][j])
                            visited.push(tGraph[tableId][j])
                        }
                    }
                }
            }
            len++;
        }
        return null
    }
    
    const dfs = (srcTable: number, destTable: number, parentMap: { [key: number]: number}) => {
        let mover = destTable
        const resp = [mover]
        if (parentMap) {
            while(parentMap[mover]) {
                queryTablesMap[mover] = parentMap[mover]
                resp.unshift(parentMap[mover])
                mover = parentMap[mover]
            }
        }
        for(let k = 0; k < resp.length; k++) {
            if (!queryTables.includes(resp[k])) {
                queryTables.push(resp[k])
            }
        }
        return resp
    }
    
    const buildFromTables = (context: ContextType) => {
        if (queryTables.length > 0) {
            if (queryTables.length > 1) {
                for (let i = 1; i < queryTables.length; i++) {
                    const left = queryTablesMap[queryTables[i]]
                    const right = queryTables[i]
                    const joinCondition = {
                        leftTable: context.tableMap[left].name,
                        rightTable: context.tableMap[right].name,
                        conditions: context.joinConditionMap[`${left}-${right}`].joinConditions,
                        type: context.joinConditionMap[`${left}-${right}`].type
                    }
                    joinConditions.push(joinCondition)
                }
            } else {
                const tab = context.tableMap[queryTables[0]].name
                joinConditions.push({
                    table: tab
                })
            }
        } else {
            throw new Error('No tables')
        }
    }
    
    function extractTableRelation(tables: number[], context: ContextType) {
        const graph = context.tableGraph
        if (tables.length > 0) {
            queryTables.push(tables[0])
            for (let i = 1; i < tables.length; i++) {
                if (!queryTables.includes(tables[i])) {
                    let minLen = Number.MAX_VALUE
                    let parentMap = null
                    let srcTable = null
                    for (let j = 0; j < queryTables.length; j++) {
                        const ret = bfs(queryTables[j], tables[i], graph)
                        if (ret && ret.len < minLen) {
                            minLen = ret.len
                            parentMap = ret.parentMap
                            srcTable = queryTables[j]
                        }
                    }
                    if (parentMap && srcTable) {
                        dfs(srcTable, tables[i], parentMap)
                    } else {
        
                    } 
                }
            }
            context.queryTables = queryTables
            context.queryTablesMap = queryTablesMap
            buildFromTables(context)
            context.joinConditions = joinConditions
            console.log('----',queryTables)
            console.log('----',queryTablesMap)
        } else {
            throw new Error('No tables available')
        }
    }


    const reset = () => {
        queryTablesMap = {}
        queryTables = []
        joinConditions = []
    }
    
    return {
        extractTableRelation,
        reset
    }
}

