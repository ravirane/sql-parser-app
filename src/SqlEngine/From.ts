import { ContextType, JoinTable, SingleTable } from './Types'

enum JOIN_TYPE {
    INNER = 'INNER',
    OUTER = 'OUTER'
}

export function from(context: ContextType) {
    const joinConditions = context.joinConditions
    const getFromTemplate = (tableReference: any) => {
        const template = {
                type: 'TableReferences',
                value: [
                    tableReference
                ]
            }
        return template
    }

    const getTableReference = (tableName: SingleTable) => {
        const template = {
            type: 'TableReference',
            value: getTableTemplate(tableName.table),
        }
        return template
    }
    
    const getTableTemplate = (table: string) => {
        const template = {
            type: 'TableFactor',
            value: {
                type: 'Identifier',
                value: table
            },
            partition: null,
            alias: null,
            hasAs: null,
            indexHintOpt: null
        }
        return template
    }

    const getJoinOption = (joinCondition: JoinTable) => {
        if (joinCondition.conditions) {
            if (joinCondition.type && ['LEFT', 'RIGHT'].includes(joinCondition.type.toUpperCase())) {
                return {
                    type: 'LeftRightJoinTable',
                    leftRight: joinCondition.type.toUpperCase(),
                    outOpt: 'OUTER',
                }
            } else {
                return {
                    type: 'InnerCrossJoinTable',
                    innerCrossOpt: "INNER",
                }
            }
        }
    }

    const getFromJoinTemplate = (joinCondition: JoinTable, leftRef?: any) => {
        const template = {
            type: 'TableReference',
            value: {
                ...getJoinOption(joinCondition),
                left: leftRef ? leftRef : getTableTemplate(joinCondition.leftTable),
                right: getTableTemplate(joinCondition.rightTable),
                condition: {
                    type: 'OnJoinCondition',
                    value: {
                        type: 'ComparisonBooleanPrimary',
                        left: {
                            "type": "Identifier",
                            "value": joinCondition.conditions[0].leftExpr.colName
                        },
                        operator: joinCondition.conditions[0].operator,
                        right: {
                            "type": "Identifier",
                            "value":  joinCondition.conditions[0].rightExpr.colName
                        }
                    }
                }
            }
        }
        return template
    }

    const getJoinTemplate = (fromTable: SingleTable | JoinTable, leftRef?: any) => {
        if (fromTable.hasOwnProperty('table')) {
            return getTableReference(fromTable as SingleTable)
        } else {
            return getFromJoinTemplate(fromTable as JoinTable, leftRef)
        }
    }


    const buildJoins = () => {
        if (joinConditions.length > 0) {
            if (joinConditions.length === 1) {
                return getFromTemplate(getJoinTemplate(joinConditions[0]))
            } else {
                let tableRef = null;
                for(let i = 0; i < joinConditions.length; i++) {
                    tableRef = getJoinTemplate(joinConditions[i], tableRef)
                }
                return getFromTemplate(tableRef)
            }
        } else {
            throw new Error('Invalid table list, expected atleast one table')
        }
    }

    return {
        buildJoins
    }
}
