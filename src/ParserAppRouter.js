import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {App} from './App';
import {TableGenerator} from './TableGenerator';
import { Layout, Menu,  } from 'antd';
const { Header, Content,  } = Layout;


export default function ParserAppRouter() {

  let urlKey = "1";  
  switch (window.location.pathname.substring(1,window.location.pathname.length)){
    case 'table-extractor':
      urlKey = "2";
      break;
  }

  debugger;
  
  return (

    <Layout className="layout">
      <Router>
        <Header>
          <div className="logo" />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[urlKey]}>
            <Menu.Item key="1"><Link to="/">Home</Link></Menu.Item>
            <Menu.Item key="2"><Link to="/table-extractor">Table Extractor</Link></Menu.Item>
          </Menu>
        </Header>    
        <Content style={{ padding: '0 50px' }}>
          <Switch>
              <Route exact path="/">
                <App />
              </Route>
              <Route path="/table-extractor">
                <TableGenerator />
              </Route>
            </Switch>
        </Content>
      </Router>
    </Layout>
  );
}

