import './App.css';
import parser from './parser/sqlParser'
import { Row, Col } from 'antd';
import { Input, Button } from 'antd';
import 'antd/dist/antd.css';
import { useState, useEffect } from 'react';
import { Engine } from './SqlEngine'
import LeftPanel from './LeftPanel'
import { DropPanel } from './DropPanel'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'
import { format } from 'sql-formatter';
import { TABLES, ATTRIBUTES, RELATIONS } from './TestData'

const { TextArea } = Input;
const sqlEngine = Engine({
  tables: TABLES,
  attributes: ATTRIBUTES,
  relations: RELATIONS
});

export function App() {
  const [sqlString, setSqlString] = useState('');
  const [jsonString, setJsonString] = useState('');
  const onJsonChange = (evt) => {
    setJsonString(evt.target.value)
  }

  useEffect(() => {
   // sqlEngine.selectAttr('Job Title')
    }, [])

  const clearLeft = () => {
    setSqlString("");
  }

  const clearRight = () => {
    setJsonString("");
  }

  const onSqlChange = (evt) => {
    setSqlString(evt.target.value)
  }

  const onGetJson = () => {
    try {
      const ast = parser.parse(sqlString)
      setJsonString(JSON.stringify(ast, null, 4))      
    } catch (error) {
      console.log(error); 
    }
  }
  
  const onGetSql = () => {
    try {
      const ast = JSON.parse(jsonString)
      setSqlString(format(parser.stringify(ast)))
    } catch (error) {
      console.log(error); 
    }
  }

  const onDropAttr = (item) => {
    sqlEngine.selectAttr(item)
    setJsonString(JSON.stringify(sqlEngine.getSql(), null, 4))     
    console.log(JSON.stringify(sqlEngine.getSql()))
  }

  useEffect(() => {
    onGetSql()
  }, [jsonString]);

  const onClearAttr = () => {
    sqlEngine.reset();
    setJsonString('')
    setSqlString('')
  }

  return (
      <div className="App">
        <DndProvider backend={HTML5Backend}>
          <div className="parser-header">SQL Parser</div>
          <Row>
            <Col span={3}>
              <LeftPanel />
            </Col>
            <Col span={4}>
              <DropPanel onDropAttr={onDropAttr} onClearAttr={onClearAttr} />
            </Col>
            <Col span={8}>
              <div className="section-header">SQL JSON</div>
              <TextArea value={jsonString} onChange={onJsonChange} className="parser-text" autoSize />
              <Button className="clear-btn" onClick={clearRight}>Clear</Button>
              <Button onClick={onGetSql}>{`Get SQL String -->`}</Button>
            </Col>
            <Col span={8}>
              <div className="section-header">SQL Query</div>
              <TextArea value={sqlString} onChange={onSqlChange} className="parser-text"  autoSize />
              <Button className="clear-btn" onClick={clearLeft}>Clear</Button>
              <Button onClick={onGetJson}>{`<-- Get JSON`}</Button>
            </Col>
          </Row>
        </DndProvider>
      </div>
  );
}
